#!/bin/bash

PORT=8321
# list of supported models: https://docs.vllm.ai/en/latest/models/supported_models.html
MODEL=mistralai/Mistral-7B-Instruct-v0.1
#MODEL=lmsys/vicuna-13b-v1.3
#MODEL=microsoft/phi-2
HOST=`hostname`
SEED=1234

. env.sh
echo Serving $MODEL on http://$HOST:$PORT
python -m vllm.entrypoints.api_server --model $MODEL --trust-remote-code --host 0.0.0.0 --port $PORT --seed $SEED
